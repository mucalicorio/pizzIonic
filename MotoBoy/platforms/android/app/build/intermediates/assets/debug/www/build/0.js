webpackJsonp([0],{

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalhesPedidoPageModule", function() { return DetalhesPedidoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detalhes_pedido__ = __webpack_require__(278);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DetalhesPedidoPageModule = /** @class */ (function () {
    function DetalhesPedidoPageModule() {
    }
    DetalhesPedidoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detalhes_pedido__["a" /* DetalhesPedidoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detalhes_pedido__["a" /* DetalhesPedidoPage */]),
            ],
        })
    ], DetalhesPedidoPageModule);
    return DetalhesPedidoPageModule;
}());

//# sourceMappingURL=detalhes-pedido.module.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetalhesPedidoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_twd_service_twd_service__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_episode__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DetalhesPedidoPage = /** @class */ (function () {
    function DetalhesPedidoPage(geolocation, twdService, navCtrl, navParams) {
        var _this = this;
        this.geolocation = geolocation;
        this.twdService = twdService;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.id = navParams.get("id");
        this.cardapioPedidos = new __WEBPACK_IMPORTED_MODULE_3__models_episode__["a" /* CardapioPedidos */]();
        this.twdService.getEpisodeByIdPedidos(this.id).then(function (data) {
            _this.obg = data;
            _this.cardapioPedidos.Cliente = _this.obg.Cliente;
            _this.cardapioPedidos.Latitude = _this.obg.Latitude;
            _this.cardapioPedidos.Longitude = _this.obg.Longitude;
            _this.cardapioPedidos.Pizza = _this.obg.Pizza;
            _this.cardapioPedidos.QuantidadePizza = _this.obg.QuantidadePizza;
            _this.cardapioPedidos.Bebida = _this.obg.Bebida;
            _this.cardapioPedidos.QuantidadeBebida = _this.obg.QuantidadeBebida;
            _this.cardapioPedidos.Total = _this.obg.Total;
            _this.cardapioPedidos.FormaPagamento = _this.obg.FormaPagamento;
            _this.cardapioPedidos.TrocoPara = _this.obg.TrocoPara;
            _this.cardapioPedidos.Entregue = _this.obg.Entregue;
            console.log(_this.cardapioPedidos);
            _this.mapLat = _this.obg.Latitude;
            _this.mapLng = _this.obg.Longitude;
            console.log(_this.mapLat + " + " + _this.mapLng);
        });
    }
    DetalhesPedidoPage.prototype.ionViewDidLoad = function () {
        var posicaoCliente = { lat: -22.1097765, lng: -50.1969425 };
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 18,
            center: posicaoCliente,
            mapTypeId: 'roadmap'
        });
        var marker = new google.maps.Marker({
            position: posicaoCliente,
            map: this.map
        });
        this.map.setCenter(posicaoCliente);
        console.log(posicaoCliente.lat);
        console.log(posicaoCliente.lng);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], DetalhesPedidoPage.prototype, "mapElement", void 0);
    DetalhesPedidoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detalhes-pedido',template:/*ion-inline-start:"C:\Users\mucal\Desktop\IoT\MotoBoy\src\pages\detalhes-pedido\detalhes-pedido.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ cardapioPedidos.Cliente }}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n    <ion-card-content>\n      <ion-card-title>\n        {{ cardapioPedidos.Cliente }} - R$ {{ cardapioPedidos.Total }}\n      </ion-card-title>\n      <b>Latitude: </b>\n      <p [innerHtml]="cardapioPedidos.Latitude"></p>\n      <b>Longitude: </b>\n      <p [innerHtml]="cardapioPedidos.Longitude"></p>\n      <b>Pizza: </b>\n      <p [innerHtml]="cardapioPedidos.Pizza"></p>\n      <b>QuantidadePizza: </b>\n      <p [innerHtml]="cardapioPedidos.QuantidadePizza"></p>\n      <b>Bebida: </b>\n      <p [innerHtml]="cardapioPedidos.Bebida"></p>\n      <b>QuantidadeBebida</b>\n      <p [innerHtml]="cardapioPedidos.QuantidadeBebida"></p>\n      <b>Total: </b>\n      <p [innerHtml]="cardapioPedidos.Total"></p>\n      <b>Forma Pagamento: </b>\n      <p [innerHtml]="cardapioPedidos.FormaPagamento"></p>\n      <b>Troco Para: </b>\n      <p [innerHtml]="cardapioPedidos.TrocoPara"></p>\n      <b>Entregue: </b>\n      <p [innerHtml]="cardapioPedidos.Entregue"></p>\n    </ion-card-content>\n  </ion-card>\n\n  <div #map id="map"></div>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\mucal\Desktop\IoT\MotoBoy\src\pages\detalhes-pedido\detalhes-pedido.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_2__providers_twd_service_twd_service__["a" /* TwdServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], DetalhesPedidoPage);
    return DetalhesPedidoPage;
}());

//# sourceMappingURL=detalhes-pedido.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardapioPedidos; });
/* unused harmony export Model */
var CardapioPedidos = /** @class */ (function () {
    function CardapioPedidos(Cliente, Latitude, Longitude, Pizza, QuantidadePizza, Bebida, QuantidadeBebida, Total, FormaPagamento, TrocoPara, Entregue) {
        this.Cliente = Cliente;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
        this.Pizza = Pizza;
        this.QuantidadePizza = QuantidadePizza;
        this.Bebida = Bebida;
        this.QuantidadeBebida = QuantidadeBebida;
        this.Total = Total;
        this.FormaPagamento = FormaPagamento;
        this.TrocoPara = TrocoPara;
        this.Entregue = Entregue;
    }
    return CardapioPedidos;
}());

;
var Model = /** @class */ (function () {
    function Model(objeto) {
        Object.assign(this, objeto);
    }
    return Model;
}());

//# sourceMappingURL=episode.js.map

/***/ })

});
//# sourceMappingURL=0.js.map