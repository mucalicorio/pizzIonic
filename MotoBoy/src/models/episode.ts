export class CardapioPedidos {
    constructor(
        public Cliente?: string,
        public Latitude?: string,
        public Longitude?: string,
        public Pizza?: string,
        public QuantidadePizza?: string,
        public Bebida?: string,
        public QuantidadeBebida?: string,
        public Total?: string,
        public FormaPagamento?: string,
        public TrocoPara?: string,
        public Entregue?: string) { }
};

export class Model {
    constructor(objeto?) {
        Object.assign(this, objeto);
    }
}