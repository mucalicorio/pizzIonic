import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TwdServiceProvider } from '../../providers/twd-service/twd-service';
import { CardapioPedidos } from '../../models/episode';
import { Geolocation } from '@ionic-native/geolocation';
declare const google;

@IonicPage()
@Component({
  selector: 'page-detalhes-pedido',
  templateUrl: 'detalhes-pedido.html',
})
export class DetalhesPedidoPage {
  public id;
  public obg: any;
  public cardapioPedidos: CardapioPedidos;
  public mapLat: any;
  public mapLng: any;

  constructor(public geolocation: Geolocation, public twdService: TwdServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.id = navParams.get("id");
    this.cardapioPedidos = new CardapioPedidos();

    this.twdService.getEpisodeByIdPedidos(this.id).then(data => {
      this.obg = data;
      this.cardapioPedidos.Cliente = this.obg.Cliente;
      this.cardapioPedidos.Latitude = this.obg.Latitude;
      this.cardapioPedidos.Longitude = this.obg.Longitude;
      this.cardapioPedidos.Pizza = this.obg.Pizza;
      this.cardapioPedidos.QuantidadePizza = this.obg.QuantidadePizza;
      this.cardapioPedidos.Bebida = this.obg.Bebida;
      this.cardapioPedidos.QuantidadeBebida = this.obg.QuantidadeBebida;
      this.cardapioPedidos.Total = this.obg.Total;
      this.cardapioPedidos.FormaPagamento = this.obg.FormaPagamento;
      this.cardapioPedidos.TrocoPara = this.obg.TrocoPara;
      this.cardapioPedidos.Entregue = this.obg.Entregue;
      console.log(this.cardapioPedidos);
      this.mapLat = this.obg.Latitude;
      this.mapLng = this.obg.Longitude;
      console.log(this.mapLat + " + " + this.mapLng)
    });
  }

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  ionViewDidLoad() {
    let posicaoCliente = { lat: -22.1097765, lng: -50.1969425 }
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 18,
      center: posicaoCliente,
      mapTypeId: 'roadmap'
    });
    var marker = new google.maps.Marker({
      position: posicaoCliente,
      map: this.map
    })
    this.map.setCenter(posicaoCliente);
    console.log(posicaoCliente.lat);
    console.log(posicaoCliente.lng);
  }
}
