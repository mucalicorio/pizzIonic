import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ListaPedidosPage } from "../lista-pedidos/lista-pedidos";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  ListaPedidos = ListaPedidosPage;

  constructor(public navCtrl: NavController) {

  }

}
