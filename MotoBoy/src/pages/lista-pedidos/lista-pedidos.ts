import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TwdServiceProvider } from '../../providers/twd-service/twd-service';

@IonicPage()
@Component({
  selector: 'page-lista-pedidos',
  templateUrl: 'lista-pedidos.html',
})
export class ListaPedidosPage {

  public obj: any;
  public data: any;
  public result: any;

  descending: boolean = false;
  order: number;
  column: string = 'Pedido';

  constructor(public twdService: TwdServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.getAll();
  }

  getAll() {
    this.twdService.loadPedidos()
      .then(data => {
        this.obj = data;
        this.result = this.obj;
      });
      console.log(JSON.stringify(this.result));
  }

  getDetalhesPedido(id:number){
    this.navCtrl.push("DetalhesPedidoPage", {
      id: id
    })
  }

  sort(){
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  ionViewDidLoad() {

  }
}
