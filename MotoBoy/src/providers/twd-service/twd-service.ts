import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";

@Injectable()
export class TwdServiceProvider {
  data: any;
  constructor(public http: Http) {
    console.log('Olá, conectado!');
  }

  loadPedidos() {
    this.data = "";
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http
        .get(
          `http://172.17.24.116:3000/Pedidos`)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data)
        })
    })
  }

  getEpisodeByIdPedidos(id: number) {
    this.data = "";
    return new Promise(resolve => {
      this.http
        .get(`http://172.17.24.116:3000/Pedidos/${id}`)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data)
        })
    })
  }
}
