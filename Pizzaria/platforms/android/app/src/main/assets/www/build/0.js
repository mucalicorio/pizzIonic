webpackJsonp([0],{

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalhesPizzaPageModule", function() { return DetalhesPizzaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detalhes_pizza__ = __webpack_require__(282);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DetalhesPizzaPageModule = /** @class */ (function () {
    function DetalhesPizzaPageModule() {
    }
    DetalhesPizzaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detalhes_pizza__["a" /* DetalhesPizzaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detalhes_pizza__["a" /* DetalhesPizzaPage */]),
            ],
        })
    ], DetalhesPizzaPageModule);
    return DetalhesPizzaPageModule;
}());

//# sourceMappingURL=detalhes-pizza.module.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CardapioPizzas; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardapioBebidas; });
/* unused harmony export Model */
var CardapioPizzas = /** @class */ (function () {
    function CardapioPizzas(Pizza, Preco, Ingredientes) {
        this.Pizza = Pizza;
        this.Preco = Preco;
        this.Ingredientes = Ingredientes;
    }
    return CardapioPizzas;
}());

;
var CardapioBebidas = /** @class */ (function () {
    function CardapioBebidas(Bebida, Volume, Preco) {
        this.Bebida = Bebida;
        this.Volume = Volume;
        this.Preco = Preco;
    }
    return CardapioBebidas;
}());

var Model = /** @class */ (function () {
    function Model(objeto) {
        Object.assign(this, objeto);
    }
    return Model;
}());

//# sourceMappingURL=episode.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetalhesPizzaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_twd_service_twd_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_episode__ = __webpack_require__(280);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DetalhesPizzaPage = /** @class */ (function () {
    function DetalhesPizzaPage(navCtrl, navParams, twdService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.twdService = twdService;
        this.id = navParams.get("id");
        this.cardapioPizzas = new __WEBPACK_IMPORTED_MODULE_3__models_episode__["b" /* CardapioPizzas */]();
        this.twdService.getEpisodeByIdPizzas(this.id).then(function (data) {
            _this.obg = data;
            _this.cardapioPizzas.Pizza = _this.obg.Pizza;
            _this.cardapioPizzas.Preco = _this.obg.Preco;
            _this.cardapioPizzas.Ingredientes = _this.obg.Ingredientes;
            console.log(_this.cardapioPizzas);
        });
    }
    DetalhesPizzaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad');
    };
    DetalhesPizzaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detalhes-pizza',template:/*ion-inline-start:"C:\Users\mucal\Desktop\IoT\PizSamu\src\pages\detalhes-pizza\detalhes-pizza.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ cardapioPizzas.Pizza }}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-card>\n    <ion-card-content>\n      <ion-card-title>\n        {{ cardapioPizzas.Pizza }} - R$ {{ cardapioPizzas.Preco }}\n      </ion-card-title>\n      <p [innerHtml]="cardapioPizzas.Ingredientes"></p>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mucal\Desktop\IoT\PizSamu\src\pages\detalhes-pizza\detalhes-pizza.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_twd_service_twd_service__["a" /* TwdServiceProvider */]])
    ], DetalhesPizzaPage);
    return DetalhesPizzaPage;
}());

//# sourceMappingURL=detalhes-pizza.js.map

/***/ })

});
//# sourceMappingURL=0.js.map