webpackJsonp([1],{

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalhesBebidaPageModule", function() { return DetalhesBebidaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detalhes_bebida__ = __webpack_require__(281);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DetalhesBebidaPageModule = /** @class */ (function () {
    function DetalhesBebidaPageModule() {
    }
    DetalhesBebidaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detalhes_bebida__["a" /* DetalhesBebidaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detalhes_bebida__["a" /* DetalhesBebidaPage */]),
            ],
        })
    ], DetalhesBebidaPageModule);
    return DetalhesBebidaPageModule;
}());

//# sourceMappingURL=detalhes-bebida.module.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CardapioPizzas; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardapioBebidas; });
/* unused harmony export Model */
var CardapioPizzas = /** @class */ (function () {
    function CardapioPizzas(Pizza, Preco, Ingredientes) {
        this.Pizza = Pizza;
        this.Preco = Preco;
        this.Ingredientes = Ingredientes;
    }
    return CardapioPizzas;
}());

;
var CardapioBebidas = /** @class */ (function () {
    function CardapioBebidas(Bebida, Volume, Preco) {
        this.Bebida = Bebida;
        this.Volume = Volume;
        this.Preco = Preco;
    }
    return CardapioBebidas;
}());

var Model = /** @class */ (function () {
    function Model(objeto) {
        Object.assign(this, objeto);
    }
    return Model;
}());

//# sourceMappingURL=episode.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetalhesBebidaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_twd_service_twd_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_episode__ = __webpack_require__(280);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DetalhesBebidaPage = /** @class */ (function () {
    function DetalhesBebidaPage(navCtrl, navParams, twdService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.twdService = twdService;
        this.id = navParams.get("id");
        this.cardapioBebidas = new __WEBPACK_IMPORTED_MODULE_3__models_episode__["a" /* CardapioBebidas */]();
        this.twdService.getEpisodeByIdBebidas(this.id).then(function (data) {
            _this.obg = data;
            _this.cardapioBebidas.Bebida = _this.obg.Bebida;
            _this.cardapioBebidas.Preco = _this.obg.Preco;
            console.log(_this.cardapioBebidas);
        });
    }
    DetalhesBebidaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetalhesBebidaPage');
    };
    DetalhesBebidaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detalhes-bebida',template:/*ion-inline-start:"C:\Users\mucal\Desktop\IoT\PizSamu\src\pages\detalhes-bebida\detalhes-bebida.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ cardapioBebidas.Bebida }}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-content>\n      <ion-card-title>\n        {{ cardapioBebidas.Bebida }} - R$ {{ cardapioBebidas.Preco }}\n      </ion-card-title>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mucal\Desktop\IoT\PizSamu\src\pages\detalhes-bebida\detalhes-bebida.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_twd_service_twd_service__["a" /* TwdServiceProvider */]])
    ], DetalhesBebidaPage);
    return DetalhesBebidaPage;
}());

//# sourceMappingURL=detalhes-bebida.js.map

/***/ })

});
//# sourceMappingURL=1.js.map