export class CardapioPizzas {
    
    constructor(
        public Pizza?: string,
        public Preco?: string,
        public Ingredientes?: string) { }
};

export class CardapioBebidas {

    constructor(
        public Bebida?: string,
        public Volume?: string,
        public Preco?: string) { }
}

export class Model {
    constructor(objeto?) {
        Object.assign(this, objeto);
    }
}
