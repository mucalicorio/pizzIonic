import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesBebidaPage } from './detalhes-bebida';

@NgModule({
  declarations: [
    DetalhesBebidaPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesBebidaPage),
  ],
})
export class DetalhesBebidaPageModule {}
