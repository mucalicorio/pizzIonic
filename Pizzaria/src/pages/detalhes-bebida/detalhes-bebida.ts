import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TwdServiceProvider } from "../../providers/twd-service/twd-service";
import { CardapioBebidas } from "../../models/episode";
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-detalhes-bebida',
  templateUrl: 'detalhes-bebida.html',
})
export class DetalhesBebidaPage {
  public id;
  public obg: any;
  public cardapioBebidas: CardapioBebidas;

  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public twdService: TwdServiceProvider) {
    this.id = navParams.get("id");
    this.cardapioBebidas = new CardapioBebidas();
    
    this.twdService.getEpisodeByIdBebidas(this.id).then(data => {
      this.obg = data;
      this.cardapioBebidas.Bebida = this.obg.Bebida;
      this.cardapioBebidas.Preco = this.obg.Preco;
      console.log(this.cardapioBebidas);
    })
  }

  adicionarPedido () {
    let alert = this.alertCtrl.create({
      title: 'Pedido Adicionado',
      inputs: [{name: 'quantidade', placeholder: 'Quantidade', type:'number'}],
      subTitle: this.id + ' - ' + this.cardapioBebidas.Bebida,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Pedido Cancelado!');
          }
        },
        {
          text: 'Confirmar',
          handler: data => {
            console.log('Pedido Adicionado! ' + data.quantidade + ' Bebidas ' + this.id);
          }
        }
      ]
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalhesBebidaPage');
  }
}
