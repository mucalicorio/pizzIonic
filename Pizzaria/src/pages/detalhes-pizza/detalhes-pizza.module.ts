import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesPizzaPage } from './detalhes-pizza';

@NgModule({
  declarations: [
    DetalhesPizzaPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesPizzaPage),
  ],
})
export class DetalhesPizzaPageModule {}
