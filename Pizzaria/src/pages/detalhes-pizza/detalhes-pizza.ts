import { Component, Type } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TwdServiceProvider } from "../../providers/twd-service/twd-service";
import { CardapioPizzas } from "../../models/episode";
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-detalhes-pizza',
  templateUrl: 'detalhes-pizza.html',
})
export class DetalhesPizzaPage {
  public id;
  public obg: any;
  public cardapioPizzas: CardapioPizzas;

  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public twdService: TwdServiceProvider) {
      this.id = navParams.get("id");
      this.cardapioPizzas = new CardapioPizzas();

      this.twdService.getEpisodeByIdPizzas(this.id).then(data => {
        this.obg = data;
        this.cardapioPizzas.Pizza = this.obg.Pizza;
        this.cardapioPizzas.Preco = this.obg.Preco;
        this.cardapioPizzas.Ingredientes = this.obg.Ingredientes;
        console.log(this.cardapioPizzas);
      });
  }

  adicionarPedido () {
    let alert = this.alertCtrl.create({
      title: 'Pedido Adicionado',
      inputs: [{name: 'quantidade', placeholder: 'Quantidade', type:'number'}],
      subTitle: this.id + ' - ' + this.cardapioPizzas.Pizza,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Pedido Cancelado!');
          }
        },
        {
          text: 'Confirmar',
          handler: data => {
            console.log('Pedido Adicionado! ' + data.quantidade + ' Pizzas ' + this.id);
          }
        }
      ]
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad');
  }
}
