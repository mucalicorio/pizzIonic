import { Component } from '@angular/core';
import { NavController, ViewController, App } from 'ionic-angular';
import { ListaPizzasPage } from "../lista-pizzas/lista-pizzas";
import { ListaBebidasPage } from "../lista-bebidas/lista-bebidas";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  ListaPizzas = ListaPizzasPage;
  ListaBebidas = ListaBebidasPage;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public appCtrl: App) {

  }
}
