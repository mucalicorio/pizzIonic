import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaBebidasPage } from './lista-bebidas';

@NgModule({
  declarations: [
    ListaBebidasPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaBebidasPage),
  ],
})
export class ListaBebidasPageModule {}
