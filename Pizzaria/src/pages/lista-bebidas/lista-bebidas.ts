import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TwdServiceProvider } from '../../providers/twd-service/twd-service';

@IonicPage()
@Component({
  selector: 'page-lista-bebidas',
  templateUrl: 'lista-bebidas.html',
})
export class ListaBebidasPage {

  public obj: any;
  public data: any;
  public result: any;

  descending: boolean = false;
  order: number;
  column: string = 'Bebida';

  constructor(public navCtrl: NavController, public twdService: TwdServiceProvider, public navParams: NavParams) {
    console.log("Helloun");
    this.getAll();
  }

  getAll() {
    console.log("Aqui");
    this.twdService.loadBebidas()
      .then(data => {
        this.obj = data;
        this.result = this.obj;
        console.log(data);
      });
  }

  getDetalhesBebida(id:number){
    this.navCtrl.push("DetalhesBebidaPage", {
      id: id
    })
  }

  sort(){
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad');
  }
}
