import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaPizzasPage } from './lista-pizzas';

@NgModule({
  declarations: [
    ListaPizzasPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaPizzasPage),
  ],
})
export class ListaPizzasPageModule {}
