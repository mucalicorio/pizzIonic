import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TwdServiceProvider } from '../../providers/twd-service/twd-service';

@IonicPage()
@Component({
  selector: 'page-lista-pizzas',
  templateUrl: 'lista-pizzas.html',
})
export class ListaPizzasPage {

  public obj: any;
  public data: any;
  public result: any;

  descending: boolean = false;
  order: number;
  column: string = 'Pizza';

  constructor(public navCtrl: NavController, public twdService: TwdServiceProvider, public navParams: NavParams) {
    this.getAll();
  }
  getAll() {
    this.twdService.loadPizzas()
      .then(data => {
        this.obj = data;
        this.result = this.obj;
      });
      console.log(JSON.stringify(this.result));
  }

  getDetalhesPizza(id:number){
    this.navCtrl.push("DetalhesPizzaPage", {
      id: id
    })
  }

  sort(){
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  ionViewDidLoad() {

  }
}
