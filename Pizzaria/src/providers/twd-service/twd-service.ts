import { Http, Headers } from "@angular/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";

@Injectable()
export class TwdServiceProvider {
  data: any;
  constructor(public http: Http) {
    console.log("Olá, conectado!");
  }

  loadPizzas() {
    this.data = "";
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http
        .get(
          `http://192.168.1.116:3000/Pizzas`)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data)
        })
    })
  }

  getEpisodeByIdPizzas(id: number) {
    this.data = "";
    return new Promise(resolve => {
      this.http
        .get(`http://192.168.1.116:3000/Pizzas/${id}`)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data)
        })
    })
  }

  loadBebidas() {
    this.data = "";
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http
        .get(
          `http://192.168.1.116:3000/Bebidas`)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data)
        })
    })
  }

  getEpisodeByIdBebidas(id: number) {
    this.data = "";
    return new Promise(resolve => {
      this.http
        .get(`http://192.168.1.116:3000/Bebidas/${id}`)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data)
        })
    })
  }

  postEpisodeByIdPedidos(id: number) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let body = {
      message: "Do you hear me?"
    };

    return new Promise(resolve => {
      this.http
        .post(`http://192.168.1.116:3000/Pedidos/${id}`, JSON.stringify(body), {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
        });
    });
  }
}
