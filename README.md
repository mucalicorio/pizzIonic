O banco de dados utilizado é uma fake api, chamada json-server.

Instale a api pelo comando "npm install -g json-server".

Acesse a pasta "database" e rode o comando "json-server --watch db.json".

O projeto "Pizzaria" seria o utilizado pelos garçons. Por enquanto, ainda não está fazendo pedidos, e sim, apenas para consulta. Estou trabalhando no desenvolvimento.

O projeto "MotoBoy" é o utilizado, como diz o nome, pelos moto boys, onde eles têm acesso aos pedidos do banco de dados (que no caso, estão sendo feitos manualmente, por enquanto).
Obs: O "tracker" do Google Maps está sendo alterado manualmente, pois não consegui passar a variável (já fiz alguns testes, mas não deu certo). Na próxima aula do Allan eu confiro com ele.